# WP Snippets

Some Quick snippets to be used in your functions.php file that may be helpful

<details><summary>Create ACF Options Page</summary>

Create ACF options page. https://www.advancedcustomfields.com/resources/options-page/

if( function_exists('acf_add_options_page') ) {

    acf_add_options_page();

}

</details>

<details><summary>Remove WYSIWYG on this page template</summary>

\*\* Remove WYSIWYG editor on this page template \*

add_action( 'admin_head', 'hide_editor' );
function hide_editor() {
\$template_file = basename( get_page_template() );

if(\$template_file == 'page.blade.php'){
remove_post_type_support('page', 'editor');
}
}

</details>

<details><summary>Remove Items from WP Admin Menu</summary>

add_action('admin_menu','remove_default_post_type');

function remove_default_post_type() {
remove_menu_page('edit.php'); //Posts
remove_menu_page('edit-comments.php'); //comments
}

</details>

<details><summary>Move Yoast Metabox to bottom of page</summary>

/_-------------------------------------
Move the Yoast SEO Meta Box to the Bottom of the edit screen in WordPress
---------------------------------------_/

function yoasttobottom() {
return 'low';
}
add_filter( 'wpseo_metabox_prio', 'yoasttobottom');

</details>

<details><summary>Remove WP Metaboxes from Dashboard</summary>

```
///Remove WP Metaboxes from Dashboard
// https://codex.wordpress.org/Function_Reference/remove_meta_box


function remove_dashboard_widgets() {
    //remove_meta_box( 'dashboard_right_now', 'dashboard', 'normal' );   // Right Now
    remove_meta_box( 'dashboard_activity', 'dashboard', 'normal' );   // Activity
	remove_meta_box( 'dashboard_recent_comments', 'dashboard', 'normal' ); // Recent Comments
	remove_meta_box( 'dashboard_incoming_links', 'dashboard', 'normal' );  // Incoming Links
	remove_meta_box( 'dashboard_plugins', 'dashboard', 'normal' );   // Plugins
	remove_meta_box( 'dashboard_quick_press', 'dashboard', 'side' );  // Quick Press
	remove_meta_box( 'dashboard_recent_drafts', 'dashboard', 'side' );  // Recent Drafts
	remove_meta_box( 'dashboard_primary', 'dashboard', 'side' );   // WordPress blog
    remove_meta_box( 'dashboard_secondary', 'dashboard', 'side' );   // Other WordPress News
    remove_meta_box('themeisle', 'dashboard', 'normal'); //themeisle plugin
    remove_meta_box('wpseo-dashboard-overview', 'dashboard', 'normal'); //yoast

	// use 'dashboard-network' as the second parameter to remove widgets from a network dashboard.
}
add_action( 'wp_dashboard_setup', 'remove_dashboard_widgets' );
```

</details>

<details><summary>Change number of returned posts in WP Query</summary>

/////////////
//Increase Returned posts to 12
////////////
function IncreasePosts($query) {
    if ($query->is_search) {
$query->set('posts_per_page',  12);
    }
    return $query;
}
add_filter('pre_get_posts','IncreasePosts');

</details>

## Sage Theme Controllet Helpers

Add these to App.php in `app/Controllers/App.php`

<details><summary>Inline SVG Helper</summary>
// Inline SVG icon helper
    // Note: Tried to make this a custom Blade directive but the argument was always converted to a string
    // Source: https://discourse.roots.io/t/best-practice-svg-wordpress-like-a-boss/6280/15
    // https://stackoverflow.com/questions/30058556/including-svg-contents-in-laravel-5-blade-template/43117258
    // https://laracasts.com/discuss/channels/laravel/custom-blade-directive-which-should-accept-an-array-of-objects

    public static function svg($params)
    {
        ob_start();
        // https://softwareengineering.stackexchange.com/questions/212995/multiple-arguments-in-function-call-vs-single-array
        $defaults = [
            'file' => '',
            'class' => null,
            'width' => null,
            'height' => null,
            // These options haven’t been integrated yet, use <span class="u-screenreader"> text instead
            // 'title' => null,
            // 'desc' => null
        ];

        $params = array_merge($defaults, $params);

        // Make sure a filename was passed
        if ( !isset($params['file']) ) {
            return false;
        }

        // Get SVG path
        // https://discourse.roots.io/t/best-practice-svg-wordpress-like-a-boss/6280/15
        $svg_path = get_template_directory() . "/assets/images/{$params['file']}.svg";

        // https://ceiwp.lndo.site/wp-content/themes/aldf/dist/images/aldf-logo-horz.svg

        // Make sure file exists
        if ( !file_exists($svg_path) ) {
            return false;
        }

        // Create DOM document to get/set attributes
        $svg = new \DOMDocument();
        $svg->load($svg_path);

        // These attributes are unnecessary when inlining the SVG
        $svg->documentElement->removeAttribute('baseProfile');
        $svg->documentElement->removeAttribute('version');
        $svg->documentElement->removeAttribute('xmlns');

        // Prevent SVG from gaining focus in IE 10+
        $svg->documentElement->setAttribute('focusable', 'false');

        // Hide SVG from screen readers (update once “title” and “desc” options are integrated)
        $svg->documentElement->setAttribute('aria-hidden', 'true');

        // Get viewbox dimensions
        $dimensions = explode(' ', $svg->documentElement->getAttribute('viewBox'));
        $boxWidth = (float)$dimensions[2];
        $boxHeight = (float)$dimensions[3];

        if (isset($params['height'])) {
            $svg->documentElement->setAttribute('height', $params['height']);

            // Automatically calculate the width if not set
            if (!isset($params['width'])) {
                $params['width'] = (float)$params['height'] * ($boxWidth / $boxHeight);
                $svg->documentElement->setAttribute('width', $params['width']);
            }
        }

        if (isset($params['width'])) {
            $svg->documentElement->setAttribute('width', $params['width']);

            // Automatically calculate the height if not set
            if (!isset($params['height'])) {
                $params['height'] = (float)$params['width'] * ($boxHeight / $boxWidth);
                $svg->documentElement->setAttribute('height', $params['height']);
            }
        }

        if(isset($params['class'])) {
            $svg->documentElement->setAttribute('class', $params['class']);
        }

        $markup = $svg->saveXML($svg->documentElement);

        echo $markup;

        $output = ob_get_contents();

        ob_end_clean();

        return $output;
    }

</details>

<details><summary>Slugify Text</summary>

function slugify($text)
{
  // replace non letter or digits by -
  $text = preg_replace('~[^\pl\d]+~u', '-', \$text);

// transliterate
$text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

// remove unwanted characters
$text = preg_replace('~[^-\w]+~', '', $text);

// trim
$text = trim($text, '-');

// remove duplicate -
$text = preg_replace('~-+~', '-', $text);

// lowercase
$text = strtolower($text);

if (empty(\$text)) {
return 'n-a';
}

return \$text;
}

</details>
